﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'BackendPluginInterface.pas' rev: 33.00 (Windows)

#ifndef BackendplugininterfaceHPP
#define BackendplugininterfaceHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>

//-- user supplied -----------------------------------------------------------

namespace Backendplugininterface
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TComputator;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TComputator : public System::TObject
{
	typedef System::TObject inherited;
	
public:
	virtual double __fastcall Compute(const double Left, const double Right) = 0 ;
public:
	/* TObject.Create */ inline __fastcall TComputator() : System::TObject() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TComputator() { }
	
};

#pragma pack(pop)

typedef System::TMetaClass* TComputatorClass;

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Backendplugininterface */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_BACKENDPLUGININTERFACE)
using namespace Backendplugininterface;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// BackendplugininterfaceHPP
