unit BackendPluginInterface;

interface

type

 TComputator = class
    public
      function Compute(const Left, Right : Double) : Double; virtual; abstract;
  end;

  TComputatorClass = class of TComputator;

implementation

end.
