unit BackendPluginManager;

interface

uses
    Classes;

type

  TBackendManager = class(TList)

  end;

  function BackendManager : TBackendManager;

implementation

var
  LocalBackendManager : TBackendManager;

function BackendManager : TBackendManager;
begin
  Result := LocalBackendManager;
end;

initialization
  LocalBackendManager := TBackendManager.Create;

finalization
  LocalBackendManager.Free;

end.
